ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
NAME := $(shell basename $(ROOT_DIR))

ifdef REG
else
REG := quay.io
endif
ifdef REG_NS
else
REG_NS := hoocloud
endif

ifdef APIKEY__QUAY_IO
#else
#APIKEY__QUAY_IO := quay.io/hoocloud
endif

MAKE := make --no-print-directory

VERSION_FILE := VERSION
VERSION := $(shell cat $(VERSION_FILE) || echo -n '0.0.0' |tee $(VERSION_FILE) )
VERSION_PARTS := $(subst ., ,$(VERSION))

MAJOR := $(word 1,$(VERSION_PARTS))
MINOR := $(word 2,$(VERSION_PARTS))
MICRO := $(word 3,$(VERSION_PARTS))

NEXT_MAJOR := $(shell echo $$(($(MAJOR)+1)))
NEXT_MINOR := $(shell echo $$(($(MINOR)+1)))
NEXT_MICRO := $(shell echo $$(($(MICRO)+1)))

NEXT_VERSION_MICRO := $(MAJOR).$(MINOR).$(NEXT_MICRO)
NEXT_VERSION_MINOR := $(MAJOR).$(NEXT_MINOR).0
NEXT_VERSION_MAJOR := $(NEXT_MAJOR).0.0

container: ver
	buildah bud --layers -t $(REG)/$(REG_NS)/$(NAME):$(NEXT_VERSION_MICRO)
	buildah tag $(REG)/$(REG_NS)/$(NAME):$(NEXT_VERSION_MICRO) $(REG)/$(REG_NS)/$(NAME):latest

.PHONY: ver
ver:
	@$(MAKE) ver-micro

.PHONY: ver-micro
ver-micro:
	@echo -n "$(NEXT_VERSION_MICRO)" > $(VERSION_FILE)
	@echo "$(NEXT_VERSION_MICRO)"

.PHONY: ver-minor
ver-minor:
	@echo -n "$(NEXT_VERSION_MINOR)" > $(VERSION_FILE)
	@echo "$(NEXT_VERSION_MINOR)"

.PHONY: ver-major
ver-major:
	@echo -n "$(NEXT_VERSION_MAJOR)" > $(VERSION_FILE)
	@echo "$(NEXT_VERSION_MAJOR)"


.PHONY: gitpush
gitpush:
	git tag $(VERSION)
	git push
	git push --tags

.PHONY: gitpushver
gitpushver: ver
	git add VERSION
	git commit -m "version $(NEXT_VERSION_MICRO)" VERSION
	git tag $(NEXT_VERSION_MICRO)
	git push
	git push --tags

.PHONY: regpush
regpush:
	curl -X POST https://$(REG)/api/v1/repository -d '{"namespace":"'$(REG_NS)'","repository":"'$(NAME)'","description":"Container image '$(NAME)'","visibility":"public"}' -H 'Authorization: Bearer '$(APIKEY__QUAY_IO)'' -H "Content-Type: application/json"
	buildah push $(REG)/$(REG_NS)/$(NAME):$(VERSION)
	buildah push $(REG)/$(REG_NS)/$(NAME):latest

.PHONY: regpushrm
regpushrm:
	## -e HOME due to bug in crun when HOME not set, see https://github.com/containers/podman/issues/9378, lmgtfy: "exec: getent: executable file not found in $PATH" crun
	## actually it seems to be a bug, that HOME is set always with runc and the container chko/docker-pushrm:1 is not built correctly
	#podman run --rm -t -v $(ROOT_DIR):/myvol -e HOME -e DOCKER_APIKEY='$(APIKEY__QUAY_IO)' docker.io/chko/docker-pushrm:1 --file /myvol/README.md --provider quay --debug $(REG)/$(REG_NS)/$(NAME)
	podman run --rm -t -v $(ROOT_DIR):/myvol -e HOME -e APIKEY__QUAY_IO='$(APIKEY__QUAY_IO)' docker.io/chko/docker-pushrm:1 --file /myvol/README.md --provider quay --debug $(REG)/$(REG_NS)/$(NAME)
